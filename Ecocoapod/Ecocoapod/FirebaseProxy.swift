//
//  FirebaseProxy.swift
//  Ecocoapod
//
//  Created by Undrakonda Gopala Krishna on 11/02/21.
//  Copyright © 2021 Undrakonda Gopala Krishna. All rights reserved.
//

import Foundation
import FirebaseAnalytics
import FirebaseCrashlytics
import FirebaseCore

final class FirebaseProxy: NSObject, FrameworksProxyProtocol {
    
    static var isDebug: Bool {
        #if DEBUG
        return true
        #else
        return false
        #endif
    }
    
    // MARK: - Methods
    func setup() {
        FirebaseApp.configure()
        
        if FirebaseProxy.isDebug {
            Crashlytics.crashlytics().setCrashlyticsCollectionEnabled(false)
        } else {
            Crashlytics.crashlytics().setCrashlyticsCollectionEnabled(true)
        }
    }
    
    func logEvent(_ name: String, parameters: [String: Any]?) {
        Analytics.logEvent(name, parameters: parameters)
    }
    
    func recordError(_ error:Error) {
        Crashlytics.crashlytics().record(error: error)
    }
    
    func setUserId(_ userId: String) {
        Analytics.setUserID(userId)
    }
    
    func recordHandledException(_ exception: NSException) {
        let  exceptionModel = ExceptionModel.init(name:exception.name.rawValue, reason:exception.reason ?? "Unknown")
        exceptionModel.stackTrace = [
        ]
        Crashlytics.crashlytics().record(exceptionModel: exceptionModel)
    }
}
