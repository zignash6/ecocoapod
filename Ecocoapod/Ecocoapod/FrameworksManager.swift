//
//  FrameworksManager.swift
//  Ecocoapod
//
//  Created by Undrakonda Gopala Krishna on 11/02/21.
//  Copyright © 2021 Undrakonda Gopala Krishna. All rights reserved.
//

import Foundation

protocol FrameworksProxyProtocol: AnyObject {
    func setup()
}

open class FrameworksManager: NSObject {
    
    static let shared = FrameworksManager()
    
    private(set) var firebaseProxy: FirebaseProxy?
    
    open var isFirebaseEnable: Bool = false
    open var isPlayMatricsEnable: Bool = false
    
    open func setupFrameworks() {
        createProxies()
        self.setupProxy(proxy: self.firebaseProxy, enabled: isFirebaseEnable)
        //self.setupProxy(proxy: self.playMetricsProxy, enabled: isPlayMatricsEnable)
    }
    
    private func createProxies() {
        firebaseProxy = FirebaseProxy()
        //playMetricsProxy = PlayMetricsProxy()
        
    }

    private func setupProxy(proxy: FrameworksProxyProtocol?, enabled: Bool?) {
        proxy?.setup()
    }
    
}

