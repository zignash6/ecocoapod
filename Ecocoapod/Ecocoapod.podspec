Pod::Spec.new do |spec|

  spec.name         = "Ecocoapod"
  spec.version      = "1.4.0"
  spec.summary      = "This is such a Ecocoapod framework.."
  spec.description  = <<-DESC
  This is some super ecocoapod that was made by a crazy guy.
                   DESC
  spec.homepage     = "https://zignash@bitbucket.org/zignash6/ecocoapod"
  spec.license      = { :type => 'MIT', :text => <<-LICENSE
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  LICENSE
  }
  spec.author       = { "undrakonda_krishna" => "undrakonda.krishna@ivycomptech.com" }
  spec.platform     = :ios, "12.0"
  spec.swift_version = "5.0"
  spec.source       = { :git => "https://zignash@bitbucket.org/zignash6/ecocoapod.git", :tag => "#{spec.version}" }
  spec.source_files  = "Ecocoapod/**/*.{h,m,swift}"

  # ――― Project Linking ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Link your library with frameworks, or libraries. Libraries do not include
  #  the lib prefix of their name.
  #

  # spec.framework  = "SomeFramework"
  # spec.frameworks = "SomeFramework", "AnotherFramework"

  # spec.library   = "iconv"
  # spec.libraries = "iconv", "xml2"


  # ――― Project Settings ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  If your library depends on compiler flags you can set them in the xcconfig hash
  #  where they will only apply to your library. If you depend on other Podspecs
  #  you can include multiple dependencies to ensure it works.

  # spec.requires_arc = true

  # spec.xcconfig = { "HEADER_SEARCH_PATHS" => "$(SDKROOT)/usr/include/libxml2" }
  # spec.dependency 'Firebase'
  spec.dependency 'Firebase/Analytics'
  spec.dependency 'Firebase/Crashlytics'
  
  

end
